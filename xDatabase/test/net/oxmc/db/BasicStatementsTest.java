package net.oxmc.db;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Date;

public class BasicStatementsTest {
    @Before
    public void setUp() throws Exception {
        Database.setupTestingEnviroment();
        DBStatement createTableStatement = new DBStatement("CREATE TABLE IF NOT EXISTS `test` (`id` INT AUTO_INCREMENT, `name` VARCHAR(45), `date` TIMESTAMP, `blob` BLOB);");
        createTableStatement.executeUpdate();
    }

    @After
    public void tearDown() throws Exception {
        Database.shutdown();
    }

    /**
     * Добавление одного элемента, два селекта, один без результатов, другой с результатом
     */
    @Test
    public void testAddGetStatement() throws SQLException {
        DBStatement statementAdd = new DBStatement("INSERT INTO `test`(`name`, `date`, `blob`) VALUES(?, ?, ?)");
        DBStatement statementGet = new DBStatement("SELECT `name`,`date`,`blob` FROM `test` WHERE `name` = ?");

        long unixTime = System.currentTimeMillis();
        byte[] blobBytes = new byte[] {8, 2, 4, 1};
        Blob blob = new SerialBlob(blobBytes);
        statementAdd.executeUpdate("limito", new Date(unixTime), blob);

        Assert.assertArrayEquals(new Object[0][0], statementGet.executeQuery("limit"));
        Assert.assertNull(statementGet.executeOneResultQuery("limit"));

        Object[][] actual = statementGet.executeQuery("limito");
        actual[0][2] = bytesFromBlob((Blob) actual[0][2]);

        Assert.assertArrayEquals(new Object[][]{{"limito", new Date(unixTime), blobBytes}}, actual);
        Assert.assertNotNull(statementGet.executeOneResultQuery("limito"));
    }

    private byte[] bytesFromBlob(Blob blob) throws SQLException {
        return blob.getBytes(0, (int) blob.length());
    }
}
