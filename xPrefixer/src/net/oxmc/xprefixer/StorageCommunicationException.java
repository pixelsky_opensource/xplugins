package net.oxmc.xprefixer;

/**
 * Ошибка при взаимодейстии с внешним хранилищем информации
 */
public class StorageCommunicationException extends Exception {
    public StorageCommunicationException() {
    }

    public StorageCommunicationException(String message) {
        super(message);
    }

    public StorageCommunicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public StorageCommunicationException(Throwable cause) {
        super(cause);
    }
}
