package net.oxmc.xprefixer;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRegisterChannelEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.io.IOException;
import java.util.logging.Level;

public class XPrefixer extends JavaPlugin
{
    static XPrefixer plugin;
    DisplayNameManager displayNameManager;
    DisplayNamesStorage storage;

    public XPrefixer() {
        plugin = this;
    }

    public void onEnable()
    {
        // Инициализация пакетов, тправляемых плагином
        this.displayNameManager = new DisplayNameManager(this);
        displayNameManager.init();

        try {
            // Подключение к БД
            this.storage = new DisplayNamesStorage();
            this.storage.init();
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, "Error while loading SQL", e);
        } catch (StorageCommunicationException e) {
            getLogger().log(Level.SEVERE, "Error while creating table", e);
        }

        // Обработчик команды
        getCommand("prefix").setExecutor(new XPrefCommandHandler());

        if (getServer().getPluginManager().getPlugin("PermissionsEx") != null)
        {
            // Интергация с префиксом PeX
            getLogger().info("xPrefix system: handle PermissionsEx");
            getServer().getPluginManager().registerEvents(new Listener() {
                @EventHandler
                public void onJoin(PlayerJoinEvent event) {
                    Player player = event.getPlayer();
                    PermissionUser user = PermissionsEx.getPermissionManager().getUser(player);
                    String ownUserPrefix = user.getOwnPrefix();
                    String prefix = ownUserPrefix != null ? ChatColor.translateAlternateColorCodes('&', ownUserPrefix) : null;
                    XPrefixer.this.displayNameManager.setPrefix(player, prefix);
                }
            }, this);
        }

        getServer().getPluginManager().registerEvents(new Listener()
        {
            @EventHandler
            public void onJoin(PlayerRegisterChannelEvent event) {
                Player player = event.getPlayer();
                try {
                    // Получение отображаемого ника из БД
                    String displayName = storage.loadDisplayName(player.getName());
                    if (displayName != null)
                        displayNameManager.setDisplayName(player.getName(), displayName);
                } catch (StorageCommunicationException e) {
                    getLogger().log(Level.SEVERE, "Error loading player's display name on join", e);
                }

                // Передача информации о нике
                displayNameManager.sendAllDisplayNames(player);
                displayNameManager.sendUpdatedName(player.getName());
            }
        }, plugin);
    }
}